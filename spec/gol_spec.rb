require 'spec_helper'

context 'game of life' do
  let(:world){World.new}
  describe 'known shapes works as they should' do
    it 'supports beacon shape' do
      world.populate_world([
                           {x: 7, y:11},
                           {x: 8, y:11},
                           {x: 7, y:10},
                           {x: 8, y:10},
                           {x: 9, y: 9},
                           {x: 10, y: 9},
                           {x: 9, y: 8},
                           {x: 10, y: 8},
      ])
      expect(world.cells.count).to be == 8
      world.tick!
      expect(world.cells.count).to be == 6
      world.tock!
      expect(world.cells.count).to be == 8
    end
    it 'supports toad shape' do
      world.populate_world([
                           {x: 2, y: 6},
                           {x: 3, y: 6},
                           {x: 4, y: 6},
                           {x: 1, y: 5},
                           {x: 2, y: 5},
                           {x: 3, y: 5}
      ])
      expect(world.cells.count).to be == 6
      world.tick_tock!
      expect(world.cells.count).to be == 6
      world.tick_tock!
      expect(world.cells.count).to be == 6
    end
    it 'supports blinker' do
      world.populate_world([
                           {x: 1, y: 2},
                           {x: 2, y: 2},
                           {x: 3, y: 2}
      ])
      expect(world.cells.count).to be == 3
      world.tick_tock!
      expect(world.cells.count).to be == 3
      world.tick_tock!
      expect(world.cells.count).to be == 3
    end
  end
end
