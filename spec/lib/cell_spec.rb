require 'spec_helper'

context :cell do
  let(:world){World.new}
  subject {Cell.new(world, 2, 7)}
  describe '#initialize' do
    it 'has coordinates' do
      expect(subject.x).to be == 2
      expect(subject.y).to be == 7
    end
    it 'has world' do
      expect(subject.world).to be_truthy
    end
  end
  describe '#spawn' do
    it 'cell spawn new cell into the world' do
      expect(subject.world.cells.count).to be == 1
      subject.spawn(2, 8)
      expect(subject.world.cells.count).to be == 2
    end
    it "cell can't be spawned on top of cell" do
      expect{subject.spawn(2, 7)}.to raise_error
    end
  end

  describe '#spawn?' do
    it 'return true if cell can be spawned at coordinate' do
      # 3, 3 should be unused.
      expect(subject.spawn?(3, 3)).to be_truthy
    end
    it 'return false if coordinate is in use by current cell' do
      expect(subject.spawn?(subject.x, subject.y)).to be_falsey
    end
  end

  describe '#die!' do
    it 'gets removed from this world' do
      world = subject.world
      object_id = subject.object_id
      expect(world.cells.map{|c| c.object_id}.include?(object_id)).to be_truthy
      subject.die!
      expect(world.cells.map{|c| c.object_id}.include?(object_id)).to be_falsey
    end
  end

  describe '#neighbors' do
    it 'with no one else in the world, there are no neighbors' do
      expect(subject.neighbors.count).to be == 0
    end
    it 'spawning a cell far off does not increase number of neighbors' do
      subject.spawn(0, 1)
      expect(subject.neighbors.count).to be == 0
    end
    it 'spawning neighbor north of cell increase neighbors with one' do
      subject.spawn(2, 8)
      expect(subject.neighbors.count).to be == 1
    end
    it 'spawning neighbor north east of cell increase neighbors with one' do
      subject.spawn(3, 8)
      expect(subject.neighbors.count).to be == 1
    end
    it 'spawning neighbor east of cell increase neighbors with one' do
      subject.spawn(3, 7)
      expect(subject.neighbors.count).to be == 1
    end
    it 'spawning neighbor south east of cell increase neighbors with one' do
      subject.spawn(3, 6)
      expect(subject.neighbors.count).to be == 1
    end
    it 'spawning neighbor south of cell increase neighbors with one' do
      subject.spawn(2, 6)
      expect(subject.neighbors.count).to be == 1
    end
    it 'spawning neighbor south west of cell increase neighbors with one' do
      subject.spawn(1, 6)
      expect(subject.neighbors.count).to be == 1
    end
    it 'spawning neighbor west of cell increase neighbors with one' do
      subject.spawn(1, 7)
      expect(subject.neighbors.count).to be == 1
    end
    it 'spawning neighbor north west of cell increase neighbors with one' do
      subject.spawn(1, 8)
      expect(subject.neighbors.count).to be == 1
    end
  end
end
