require 'spec_helper'

context :world do
  subject{World.new}
  describe '#cells' do
    it 'new world cells is an array' do
      expect(subject.cells.instance_of?(Array)).to be_truthy
    end
  end
  describe '#populate_world' do
    it 'populates world with cell from coordinates in argument' do
      expect(subject.cells.count).to be == 0
      coordinates = [
        {x: 2, y: 2},
        {x: 1, y: 2},
        {x: 5, y: 8},
        {x: 5, y: 9},
        {x: 5, y: 10},
        {x: 4, y: 8},
        {x: 2, y: 1}
      ]
      subject.populate_world(coordinates)
      expect(subject.cells.count).to be == 7
    end
  end
  context "Any live cell with fewer than two live neighbours dies, as if caused by under-population." do
    describe '#tick!' do
      it 'live cell with zero neighbors dies' do
        subject.populate_world([{x: 4, y: 6}])
        subject.tick!
        expect(subject.cells.count).to be == 0
      end
      it 'live cell with one neighbor dies' do
        subject.populate_world([
                               {x: 4, y: 6},
                               {x: 4, y: 7}
        ])
        subject.tick!
        expect(subject.cells.count).to be == 0
      end
    end
  end
  context "Any live cell with two or three live neighbours lives on to the next generation." do
    describe '#tick!' do
      it 'live cell with two neighbors survives' do
        subject.populate_world([
                               {x: 4, y: 6}, # Two neighbors.
                               {x: 5, y: 6}, # Two neighbors.
                               {x: 4, y: 7}  # Two neighbors.
        ])
        subject.tick!
        expect(subject.cells.count).to be == 3
      end
      it 'live cell with three neighbors survives' do
        subject.populate_world([
                               {x: 4, y: 6}, # Three neighbors.
                               {x: 5, y: 6}, # Three neighbors.
                               {x: 5, y: 7}, # Three neighbors.
                               {x: 4, y: 7}  # Three neighbors.
        ])
        subject.tick!
        expect(subject.cells.count).to be == 4
      end
    end
  end
  context "Any live cell with more than three live neighbours dies, as if by overcrowding." do
    describe '#tick!' do
      it 'live cell with four neighbors dies' do
        subject.populate_world([
                               {x: 4, y: 6}, # Three neighbors.
                               {x: 5, y: 6}, # Four neighbors.
                               {x: 5, y: 7}, # Four neighbors.
                               {x: 4, y: 7}, # Three neighbors.
                               {x: 6, y: 7}  # Two neighbors.
        ])
        subject.tick!
        expect(subject.cells.count).to be == 3
      end
      it 'live cell with five neighbors dies' do
        subject.populate_world([
                               {x: 4, y: 6}, # Four neighbors.
                               {x: 5, y: 6}, # Five neighbors.
                               {x: 5, y: 7}, # Four neighbors.
                               {x: 4, y: 7}, # Three neighbors.
                               {x: 6, y: 7}, # Two neighbors.
                               {x: 5, y: 5}  # Two neighbors.
        ])
        subject.tick!
        expect(subject.cells.count).to be == 3
      end
      it 'live cell with six neighbors dies' do
        subject.populate_world([
                               {x: 4, y: 6}, # Five neighbors.
                               {x: 5, y: 6}, # Six neighbors.
                               {x: 5, y: 7}, # Four neighbors.
                               {x: 4, y: 7}, # Three neighbors.
                               {x: 6, y: 7}, # Two neighbors.
                               {x: 5, y: 5}, # Three neighbors.
                               {x: 4, y: 5}  # Three neighbors.
        ])
        subject.tick!
        expect(subject.cells.count).to be == 4
      end
      it 'live cell with seven neighbors dies' do
        subject.populate_world([
                               {x: 4, y: 6}, # Five neighbors.
                               {x: 5, y: 6}, # Seven neighbors.
                               {x: 5, y: 7}, # Four neighbors.
                               {x: 4, y: 7}, # Three neighbors.
                               {x: 6, y: 7}, # Two neighbors.
                               {x: 5, y: 5}, # Four neighbors.
                               {x: 6, y: 5}, # Two neighbors.
                               {x: 4, y: 5}  # Three neighbors.
        ])
        subject.tick!
        expect(subject.cells.count).to be == 4
      end
      it 'live cell with eight neighbors dies' do
        subject.populate_world([
                               {x: 4, y: 6}, # Five neighbors.
                               {x: 5, y: 6}, # Eight neighbors.
                               {x: 5, y: 7}, # Five neighbors.
                               {x: 4, y: 7}, # Three neighbors.
                               {x: 6, y: 7}, # Three neighbors.
                               {x: 5, y: 5}, # Five neighbors.
                               {x: 6, y: 5}, # Three neighbors.
                               {x: 6, y: 6}, # Five neighbors.
                               {x: 4, y: 5}  # Three neighbors.
        ])
        subject.tick!
        expect(subject.cells.count).to be == 4
      end
    end

    context "Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction." do
      context "Need to calculate virtual size of world to find dead cells" do
        describe '#virtual_x_range' do
          it 'returns virtual x range equal (0..0) when world have no cells' do
            expect(subject.virtual_x_range).to eq((0..0))
          end
          it 'returns range equal (0..2) when a cell at x=1' do
            subject.populate_world([{x:1, y:2}])
            expect(subject.virtual_x_range).to eq((0..2))
          end
          it 'returns range equal (0..7) when cell at x=1 and x=6' do
            subject.populate_world([{x:1, y:2}, {x:6, y:2}])
            expect(subject.virtual_x_range).to eq((0..7))
          end
        end
        describe '#virtual_y_range' do
          it 'returns virtual y range equal (0..0) when world have no cells' do
            expect(subject.virtual_y_range).to eq((0..0))
          end
          it 'returns range equal (1..3) when a cell at y=2' do
            subject.populate_world([{x:1, y:2}])
            expect(subject.virtual_y_range).to eq((1..3))
          end
          it 'returns range equal (1..7) when cell at y=2 and y=6' do
            subject.populate_world([{x:1, y:2}, {x:1, y:6}])
            expect(subject.virtual_y_range).to eq((1..7))
          end
        end
      end
      describe '#virtual_status_map' do
        it 'empty if no cells' do
          expect(subject.virtual_status_map.empty?).to be_truthy
        end
        it 'grows as cells gets added' do
          subject.populate_world([{x: 2, y:4}])
          expect(subject.virtual_status_map.count).to be == 3*3
          subject.populate_world([{x: 4, y:6}])
          expect(subject.virtual_status_map.count).to be == (3+2)*(3+2)
        end
      end
      describe '#tock!' do
        it 'populates empty space when there are three neighbors' do
          subject.populate_world([{x:1, y:3}, {x:3, y:4}, {x:1, y:5}])
          expect(subject.cells.count).to be == 3
          subject.tock!
          expect(subject.cells.count).to be == 4
        end
      end
    end
  end
end
