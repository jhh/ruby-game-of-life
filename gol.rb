require './lib/world'
require './lib/cell'

LOOP = 45
TICKS = 1
MSG = <<-TXT
 Conway's
    Game Of Life

 by JHH              %i
-----------------------
TXT

world = World.new
world.populate_world([

                     # Beacon.
                     {x: 7, y:11},
                     {x: 8, y:11},
                     {x: 7, y:10},
                     {x: 8, y:10},
                     {x: 9, y: 9},
                     {x: 10, y: 9},
                     {x: 9, y: 8},
                     {x: 10, y: 8},

                     # Toad.
                     {x: 2, y: 6},
                     {x: 3, y: 6},
                     {x: 4, y: 6},
                     {x: 1, y: 5},
                     {x: 2, y: 5},
                     {x: 3, y: 5},

                     # Blinker.
                     {x: 1, y: 1},
                     {x: 2, y: 1},
                     {x: 3, y: 1},

                     # Glider (hacker emblem).
                     {x: 7, y: 3},
                     {x: 8, y: 3},
                     {x: 7, y: 2},
                     {x: 9, y: 2},
                     {x: 7, y: 1}

])

LOOP.times do |i|
  system 'clear'
  print MSG % (LOOP - i)
  map = world.virtual_status_map
  row = map.first[:y]
  width = 0
  map.each do |s|
    if row != s[:y]
      row = s[:y]
      print ".\n"
      width = 0
    end
    if s[:status] == :dead
      print "  "
      width += 2
    elsif s[:status] == :alive
      print "<>"
      width += 2
    end
  end
  world.tick_tock!
  sleep TICKS
end

system 'clear'
