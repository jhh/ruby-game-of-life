class World
  attr_accessor :map
  def initialize
    @map = []
  end

  def cells
    result = []
    @map.sort{|a,b| b[:y] <=> a[:y]}.each do |r|
      row = r[:x].sort{|a,b| a.x <=> b.x}
      result += row
    end
    result
  end

  def virtual_x_range
    min = nil
    max = nil
    cells.each do |c|
      min ||= c.x
      min = c.x if min > c.x
      max ||= c.x
      max = c.x if max < c.x
    end
    return (0..0) if min.nil? || max.nil?
    (min-1)..(max+1)
  end

  def virtual_y_range
    min = nil
    max = nil
    cells.each do |c|
      min ||= c.y
      min = c.y if min > c.y
      max ||= c.y
      max = c.y if max < c.y
    end
    return (0..0) if min.nil? || max.nil?
    (min-1)..(max+1)
  end

  # Generate a virtual status map,
  # with unpopulated cells in all the edges.
  def virtual_status_map
    x_range = virtual_x_range
    y_range = virtual_y_range
    result = []
    return result if x_range.count == 1 || y_range.count == 1
    # Starts with first row (highest y value).
    # This make the world easier to print out and read.
    (y_range.last).downto(y_range.first).each do |y|
      # Continue with each column, from low to high.
      row = map.select{|r| r[:y] == y}.first
      for x in x_range do
        status = :dead
        if row && !row[:x].select{|c| c.x == x}.empty?
          status = :alive
        end
        result << {status: status, x: x, y: y}
      end
    end
    result
  end

  def populate_world(coordinates)
    coordinates.each do |c|
      Cell.new(self, c[:x], c[:y])
    end
  end

  def tick_tock!
    death_row = tick
    new_born = tock
    death_row.each do |c|
      c.die!
    end
    populate_world(new_born)
  end

  # Clock ticks, who survives, who dies.
  def tick
    death_row = []
    cells.each do |c|
      cell_neighbors = c.neighbors.count
      unless (0..8).include?(cell_neighbors)
        raise 'This world have limitations'
      end
      case cell_neighbors
      when 0,1,4,5,6,7,8
        death_row << c
      end
    end
    death_row
  end

  def tick!
    death_row = tick
    death_row.each do |c|
      c.die!
    end
  end

  # Clock tocks, new cell magically appears
  # where spot has exactly three neighbors.
  def tock
    vmap = virtual_status_map
    dead = vmap.select{|s| s[:status] == :dead}
    alive = vmap - dead
    new_born = []
    dead.each do |d|
      if virtual_neighbors(alive, d) == 3
        new_born << d
      end
    end
    new_born
  end

  def tock!
    new_born = tock
    populate_world(new_born)
  end

  def virtual_neighbors(virtual_list, virtual_status)
    x = virtual_status[:x]
    y = virtual_status[:y]
    neighbors = virtual_list.select do |c|
     if c[:x] == x && c[:y] == y + 1
       true # Cell in north is a neighbor.
     elsif c[:x] == x + 1 && c[:y] == y + 1
       true # Cell in north east is a neighbor.
     elsif c[:x] == x + 1 && c[:y] == y
       true # Cell in east is a neighbor.
     elsif c[:x] == x + 1 && c[:y] == y - 1
       true # Cell in south east is a neighbor.
     elsif c[:x] == x && c[:y] == y - 1
       true # Cell in south is a neighbor.
     elsif c[:x] == x - 1 && c[:y] == y - 1
       true # Cell in south west is a neighbor.
     elsif c[:x] == x - 1 && c[:y] == y
       true # Cell in west is a neighbor.
     elsif c[:x] == x - 1 && c[:y] == y + 1
       true # Cell in north west is a neighbor.
     else
       false
     end
    end
    neighbors.count
  end
end
