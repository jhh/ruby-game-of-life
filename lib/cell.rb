class Cell
  attr_accessor :world, :x, :y

  def initialize(world, x, y)
    @world, @x, @y = world, x, y
    unless spawn?(x, y)
      msg = "Cell does not have super power in this world."
      raise ArgumentError.new(msg)
    end
    row = @world.map.select{|r| r[:y] == y}.first
    if row.nil?
      row = {y: y, x: []}
      @world.map << row
    end
    row[:x] << self
  end

  def spawn(x, y)
    self.class.new(self.world, x, y)
  end

  def spawn?(x, y)
    row = @world.map.select{|r| r[:y] == y}.first
    return true unless row
    row[:x].select{|c| c.x == x}.empty?
  end

  def die!
    @world.map.select{|r| r[:y] == y}.first[:x] -= [self]
  end

  def neighbors
    @world.cells.select do |c|
     if c.x == self.x && c.y == self.y + 1
       true # Cell in north is a neighbor.
     elsif c.x == self.x + 1 && c.y == self.y + 1
       true # Cell in north east is a neighbor.
     elsif c.x == self.x + 1 && c.y == self.y
       true # Cell in east is a neighbor.
     elsif c.x == self.x + 1 && c.y == self.y - 1
       true # Cell in south east is a neighbor.
     elsif c.x == self.x && c.y == self.y - 1
       true # Cell in south is a neighbor.
     elsif c.x == self.x - 1 && c.y == self.y - 1
       true # Cell in south west is a neighbor.
     elsif c.x == self.x - 1 && c.y == self.y
       true # Cell in west is a neighbor.
     elsif c.x == self.x - 1 && c.y == self.y + 1
       true # Cell in north west is a neighbor.
     else
       false
     end
    end
  end
end
